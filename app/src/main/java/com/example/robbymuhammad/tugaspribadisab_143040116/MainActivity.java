package com.example.robbymuhammad.tugaspribadisab_143040116;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    String arrMenu[] = {"Kaos","Jaket","Celana","Tentang Konveksi"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView) ;
        listView.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, arrMenu));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int
                    position, long id) {
                switch (position) {
                    case 0 : startActivity(new Intent(MainActivity.this, KaosActivity.class)) ; break;
                    case 1 : startActivity(new Intent(MainActivity.this, JaketActivity.class)) ; break;
                    case 2 : startActivity(new Intent(MainActivity.this, CelanaActivity.class)) ; break;
                    case 3 : startActivity(new Intent(MainActivity.this, TentangKonveksiActivity.class)) ; break;
                }
            }
        });
    }
}
