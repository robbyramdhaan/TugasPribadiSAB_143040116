package com.example.robbymuhammad.tugaspribadisab_143040116;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Created by Robby Muhammad R on 11-May-17.
 */

public class ImageAdapterKaos extends BaseAdapter {
    private Context mContext;

    // Keep all Images in array
    public Integer[] mThumbIds = {
            R.drawable.kaos1, R.drawable.kaos6, R.drawable.kaos11,
            R.drawable.kaos2, R.drawable.kaos7, R.drawable.kaos12,
            R.drawable.kaos3, R.drawable.kaos8, R.drawable.kaos13,
            R.drawable.kaos4, R.drawable.kaos9, R.drawable.kaos14,
            R.drawable.kaos5, R.drawable.kaos10, R.drawable.kaos15,
    };

    // Constructor
    public ImageAdapterKaos(Context c) {
        mContext = c;
    }

    @Override
    public int getCount() {
        return mThumbIds.length;
    }

    @Override
    public Object getItem(int position) {
        return mThumbIds[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);
        imageView.setImageResource(mThumbIds[position]);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new GridView.LayoutParams(70, 70));
        return imageView;
    }
}
