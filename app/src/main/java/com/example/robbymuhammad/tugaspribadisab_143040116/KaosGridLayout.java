package com.example.robbymuhammad.tugaspribadisab_143040116;

import android.app.Activity;
import android.os.Bundle;
import android.widget.GridView;

/**
 * Created by Robby Muhammad R on 11-May-17.
 */

public class KaosGridLayout extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kaosgridlayout);

        GridView gridView = (GridView) findViewById(R.id.grid_view);

        // Instance of ImageAdapter Class
        gridView.setAdapter(new ImageAdapterKaos(this));
    }
}
